package com.company;

import java.util.ArrayList;

public class Parking {
    private int numOfFreeParkingPlaces;

    private final int numOfLORRYParkingPlaces;
    private final int numOfPASSParkingPlaces;

    private int numOfFreeLORRYParkingPlaces;
    private int numOfFreePASSParkingPlaces;

    private final ArrayList<Integer> arrayOfParkingPlaces = new ArrayList<>();

    public Parking(int pasParkingPlace, int lorryParkingPlaces) throws Exception {
        int numOfParkingPlaces = pasParkingPlace + lorryParkingPlaces;
        if (numOfParkingPlaces < 1) {
            throw new Exception("The number of parking spaces cannot be limited than 1!");
        } else {
            this.numOfLORRYParkingPlaces = lorryParkingPlaces;
            this.numOfPASSParkingPlaces = pasParkingPlace;

            this.numOfFreeParkingPlaces = pasParkingPlace + lorryParkingPlaces;

            this.numOfFreeLORRYParkingPlaces = lorryParkingPlaces;
            this.numOfFreePASSParkingPlaces = pasParkingPlace;
        }
    }

    public void pasPlaceTaked() {
        numOfFreePASSParkingPlaces--;
        numOfFreeParkingPlaces--;
    }

    public void lorryPlaceTaked() {
        numOfFreeLORRYParkingPlaces--;
        numOfFreeParkingPlaces--;
    }

    public void pasPlaceFree() {
        numOfFreeParkingPlaces++;
        numOfFreeParkingPlaces++;
    }

    public void lorryPlaceFree() {
        numOfFreeLORRYParkingPlaces++;
        numOfFreeParkingPlaces++;
    }

    public int getNumOfFreeParkingPlaces() {
        return numOfFreeParkingPlaces;
    }

    public int getNumOfLORRYParkingPlaces() {
        return numOfLORRYParkingPlaces;
    }

    public int getNumOfPASSParkingPlaces() {
        return numOfPASSParkingPlaces;
    }

    public int getNumOfFreeLORRYParkingPlaces() {
        return numOfFreeLORRYParkingPlaces;
    }

    public int getNumOfFreePASSParkingPlaces() {
        return numOfFreePASSParkingPlaces;
    }

    public ArrayList<Integer> getArrayOfParkingPlaces() {
        return arrayOfParkingPlaces;
    }

    public void fillArrayOfPP() {
        arrayOfParkingPlaces.clear();
        for (int i = 0; i < numOfFreeLORRYParkingPlaces; i++) {
            arrayOfParkingPlaces.add(20);
        }
        for (int i = 0; i < numOfFreePASSParkingPlaces; i++) {
            arrayOfParkingPlaces.add(0);
        }
    }
}

