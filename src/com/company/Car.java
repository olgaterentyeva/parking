package com.company;

public class Car {
    private int moves;
    private final int id;
    private int indexOnParking;
    private int specialIndexOnParking;

    enum Type {PASSANGER, LORRY}

    private final Type type;

    public Car(int moves, int id, Type type) {
        this.moves = moves;
        this.id = id;
        this.type = type;
    }

    public int getIndexOnParking() {
        return indexOnParking;
    }

    public void setIndexOnParking(int indexOnParking) {
        this.indexOnParking = indexOnParking;
    }

    public int getSpecialIndexOnParking() {
        return specialIndexOnParking;
    }

    public void setSpecialIndexOnParking(int specialIndexOnParking) {
        this.specialIndexOnParking = specialIndexOnParking;
    }

    public int getId() {
        return id;
    }

    public int getMoves() {
        return moves;
    }

    public Type getType() {
        return type;
    }

    public void doStep() {
        this.moves --;
    }

    @Override
    public String toString() {
        return (type == Type.PASSANGER ? "Passenger car " : "Lorry ") + "with ID " + id + " moves left: " + moves;
    }
}

