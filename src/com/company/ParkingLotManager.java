package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class ParkingLotManager {
    static ArrayList<Integer> unicID = new ArrayList<>();
    static ArrayList<Car> waitingCars = new ArrayList<>();
    static ArrayList<Car> carsOnParking = new ArrayList<>();
    static ArrayList<Car> leftingCars = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        Scanner command = new Scanner(System.in);
        Scanner scanner1 = new Scanner(System.in);

        System.out.println("Welcome to the parking lot PARK & FLY!");
        System.out.println("Enter the number of Passenger Cars and Lorries parking spaces, respectively. Through ENTER");
        Parking parking = new Parking(scanner.nextInt(), scanner1.nextInt());

        System.out.println("List of commands: \nz - make a move, \nx - get information about parking spaces, \nc - clear the parking lot");

        parking.fillArrayOfPP();
        createWaitingCars(parking.getNumOfPASSParkingPlaces(), parking.getNumOfLORRYParkingPlaces());


        while (true) {
            if (parking.getNumOfFreeParkingPlaces() == 0) {
                System.out.println("There are no available spaces!");
                System.out.println("Space will be available in " + whenFree(carsOnParking) + " moves.");
            }

            System.out.println("Enter the command >> ");


            switch (command.nextLine()) {
                case ("z"):
                    while (parking.getNumOfFreeParkingPlaces() > 0) {
                        Car firstLORcar = getFirstLORcar(waitingCars);
                        Car firstPAScar = getFirstPAScar(waitingCars);
                        Car firstWcar = waitingCars.get(0);

                        switch (firstWcar.getType()) {


                            case PASSANGER:

                                if (parking.getArrayOfParkingPlaces().contains(0)) {
                                    System.out.println("Passenger car with ID " + firstWcar.getId() + " pulls into the parking lot!");
                                    carsOnParking.add(firstWcar);
                                    firstWcar.setIndexOnParking(parking.getArrayOfParkingPlaces().indexOf(0));
                                    waitingCars.remove(firstWcar);
                                    parking.pasPlaceTaked();
                                    parking.getArrayOfParkingPlaces().set(parking.getArrayOfParkingPlaces().indexOf(0), 1);
                                } else {
                                    assert firstLORcar != null;
                                    System.out.println("Lorry with ID " + firstLORcar.getId() + " pulls into the parking lot!");
                                    carsOnParking.add(firstLORcar);
                                    firstLORcar.setIndexOnParking(parking.getArrayOfParkingPlaces().indexOf(20));
                                    waitingCars.remove(firstLORcar);
                                    parking.lorryPlaceTaked();
                                    parking.getArrayOfParkingPlaces().set(parking.getArrayOfParkingPlaces().indexOf(20), 2);
                                }
                                break;

                            case LORRY:

                                if (parking.getArrayOfParkingPlaces().contains(20)) {
                                    System.out.println("Lorry " + "with ID " + firstWcar.getId() + " pulls into the parking lot!");
                                    carsOnParking.add(firstWcar);
                                    firstWcar.setIndexOnParking(parking.getArrayOfParkingPlaces().indexOf(20));
                                    waitingCars.remove(firstWcar);
                                    parking.lorryPlaceTaked();
                                    parking.getArrayOfParkingPlaces().set(parking.getArrayOfParkingPlaces().indexOf(20), 2);
                                } else if
                                (whenContain(parking.getArrayOfParkingPlaces()) > -1) {
                                    System.out.println("Lorry " +  "with ID " + firstWcar.getId() + " pulls into the parking lot and takes 2 passenger car spaces!");
                                    carsOnParking.add(firstWcar);
                                    firstWcar.setSpecialIndexOnParking(whenContain(parking.getArrayOfParkingPlaces()));
                                    waitingCars.remove(firstWcar);

                                    int f = whenContain(parking.getArrayOfParkingPlaces());
                                    parking.getArrayOfParkingPlaces().set(f, 22);
                                    parking.getArrayOfParkingPlaces().set(f + 1, 22);
                                    parking.pasPlaceTaked();
                                    parking.pasPlaceTaked();
                                } else {
                                    assert firstPAScar != null;
                                    System.out.println("Passenger car" +  "with ID " + firstPAScar.getId() + " pulls into the parking lot!");
                                    carsOnParking.add(firstPAScar);
                                    firstPAScar.setIndexOnParking(parking.getArrayOfParkingPlaces().indexOf(0));
                                    waitingCars.remove(firstPAScar);
                                    parking.pasPlaceTaked();
                                    parking.getArrayOfParkingPlaces().set(parking.getArrayOfParkingPlaces().indexOf(0), 1);
                                }
                                break;
                            default:
                        }
                    }

                    for (Car el : carsOnParking) {
                        el.doStep();

                        if (el.getMoves() <= 0) {
                            leftingCars.add(el);
                            System.out.println((el.getType() == Car.Type.PASSANGER ? "Passenger car " : "Lorry ") + "with  ID = " + el.getId() + " leaving the parking lot!");
                        }
                    }
                    for (Car leftC : leftingCars) {
                        if (leftC.getSpecialIndexOnParking() > -1) {
                            int index = leftC.getSpecialIndexOnParking();
                            parking.getArrayOfParkingPlaces().set(index, 0);
                            parking.getArrayOfParkingPlaces().set(index + 1, 0);
                            createWaitingCars(parking.getNumOfPASSParkingPlaces() / 3, parking.getNumOfLORRYParkingPlaces() / 3);
                            parking.pasPlaceFree();
                            parking.pasPlaceFree();
                            carsOnParking.remove(leftC);
                        } else {
                            parking.getArrayOfParkingPlaces().set(leftC.getIndexOnParking(), (leftC.getType() == Car.Type.PASSANGER) ? 0 : 20);
                            createWaitingCars(parking.getNumOfPASSParkingPlaces() / 3, parking.getNumOfLORRYParkingPlaces() / 3);
                            if ((leftC.getType() == Car.Type.PASSANGER)) {
                                parking.pasPlaceFree();
                            } else {
                                parking.lorryPlaceTaked();
                            }
                            carsOnParking.remove(leftC);
                        }
                    }
                    leftingCars.clear();

                    for (Car el : carsOnParking)
                        System.out.println(el);
                    break;


                case ("x"):
                    if (carsOnParking.isEmpty())
                        System.out.println("Parking lot is empty!");
                    else {
                        int numOfPASTakedPlaces = parking.getNumOfPASSParkingPlaces() - parking.getNumOfFreePASSParkingPlaces();
                        int numOfLORTakedPlaces = parking.getNumOfLORRYParkingPlaces() - parking.getNumOfFreeLORRYParkingPlaces();

                        System.out.println("Passenger seats: \noccupied >> " + numOfPASTakedPlaces + "\nfree >> " + parking.getNumOfFreePASSParkingPlaces());
                        System.out.println("Lorry seats: \noccupied >> " + numOfLORTakedPlaces + "\nfree >> " + parking.getNumOfFreeLORRYParkingPlaces());
                        System.out.println("Space will be available in " + whenFree(carsOnParking) + " moves");

                        for (Car car : carsOnParking) {
                            if (car.getSpecialIndexOnParking() > -1)
                                System.out.println("Lorry with ID " + car.getId() + " takes 2 passenger car spaces " + "moves left: " + car.getMoves());
                            else
                                System.out.println(car.getType() == Car.Type.PASSANGER ? "Passenger car " : "Lorry " + "with ID " + car.getId() + "takes " + (car.getType() == Car.Type.PASSANGER ? "Passenger " : "Lorry" + "space," + "moves left: " + car.getMoves()));
                        }
                    }
                    break;


                case ("c"):
                    for (int i = 0; i < carsOnParking.size(); i++) {
                        if (carsOnParking.get(i).getType() == Car.Type.PASSANGER)
                            parking.pasPlaceFree();
                        else if
                        (carsOnParking.get(i).getSpecialIndexOnParking() > -1) {
                            parking.pasPlaceFree();
                            parking.pasPlaceFree();
                        } else
                            parking.lorryPlaceFree();
                    }
                    carsOnParking.clear();
                    createWaitingCars(parking.getNumOfPASSParkingPlaces(), parking.getNumOfLORRYParkingPlaces());
                    parking.fillArrayOfPP();
                    System.out.println("Parking lot is cleared!");
                    break;

                default:
                    System.out.println("There no such command!");
                    System.out.println("List of commands: \nz - make a move, \nx - get information about parking spaces, \nc - clear the parking lot");
                    break;
            }
        }
    }

    public static void createWaitingCars (int pvalue, int lvalue) {
        for (int i = 0; i < pvalue; i++) {
            int random_number1 = 1000 + (int) (Math.random() * 9000);
            int random_number2 = 2 + (int) (Math.random() * 10);
            while (unicID.contains(random_number1)) {
                random_number1 = 1000 + (int) (Math.random() * 9000);
            }
            unicID.add(random_number1);
            waitingCars.add(new Car(random_number2, random_number1, Car.Type.PASSANGER));
        }

        for (int i = 0; i < lvalue; i++) {
            int random_number1 = 1000 + (int) (Math.random() * 9000);
            int random_number2 = 2 + (int) (Math.random() * 10);
            while (unicID.contains(random_number1)) {
                random_number1 = 1000 + (int) (Math.random() * 9000);
            }
            unicID.add(random_number1);
            waitingCars.add(new Car(random_number2, random_number1, Car.Type.LORRY));
        }
    }

    public static int whenFree (ArrayList<Car>cars) {
        ArrayList<Integer> numOfMoves = new ArrayList<>();
        for (Car el : cars) {
            numOfMoves.add(el.getMoves());
        }
        int buf = Collections.min(numOfMoves);
        numOfMoves.clear();
        return buf;
    }

    public static int whenContain (ArrayList <Integer> list) {
        int value = -1;

        for (int i = 0; i < list.size()-1; i++) {
            if ((list.get(i)==0) && (list.get(i+1) == 0)) {
                value = i;
                break;
            }
        }
        return value;
    }

    public static Car getFirstPAScar (ArrayList<Car> list) {
        for (Car el: list) {
            if (el.getType()== Car.Type.PASSANGER)
                return el;
        }
        return null;
    }

    public static Car getFirstLORcar (ArrayList<Car> list) {
        for (Car el: list) {
            if (el.getType() == Car.Type.LORRY)
                return el;
        }
        return null;
    }
}

